# Wilson

A 2-day group project developed over the weekend.

## The Idea

A turn based multiplayer survival game where cooperation is key in order to survive and escape a deserted island.

## The Development

This game was based on a client/server architecture featuring a lobby and a game stage. The game logic core was structured using the strategy design pattern in order to access and execute player actions. This provided scalability of a whole higher order of magnitude.
This project was brought to be with the combined use of Kanban methodology and short sprints during its development.

## Tech && Methodologies used

Java, IntelliJ, Linux, Mac OS, Prompt-View Library, Maven, Git, Kanban.

## Authors

* **Carlos Cardoso** - [Carlosarc1981](https://github.com/Carlosarc1981)
* **Eduardo Marques** - [edcm95](https://github.com/edcm95)
* **Ricardo Dias** - [RicardoSnaw](https://github.com/RicardoSnaw)
* **Rui Rodrigues** - [carcaso](https://github.com/carcaso)
* **Vando Clemente** - [Vonderr](https://github.com/Vonderr)


# Thank you!
